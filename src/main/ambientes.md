AMBIENTES
=========

### Definição

Ambientes são entendidos como qualquer lugar ou sistema que pode  
alternar entre estado de ativo e desativo.

Exemplos: Sistema de alarme, Luzes de ambiente, Porta da garagem.

Ambientes são identificados por um numero inteiro que deve ser passado
entre o WebServer e o Arduino.

### Tabela

AMBIENTE		| QUALIFICADOR | IDENTIFICADOR
:------:		| :----------: | :------------:
Sala			| Luz		   | 0
Banheiro A		| Luz		   | 1
Banheiro B		| Luz		   | 2
Quarto A		| Luz		   | 3
Quarto B		| Luz		   | 4
Escritorio		| Luz		   | 5
Corredor		| Luz		   | 6
Garagem			| Luz		   | 7
Churrasco		| Luz		   | 8
Intruso			| Alarme	   | 9
Garagem Porta	| Porta		   | 10
DADOS SENSORES	| Dados		   | 11


