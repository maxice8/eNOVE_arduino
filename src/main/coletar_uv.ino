/**
 * @file
 * @author maxice8
 * @date 30/11/2018
 * @brief funções para lidar com sensor de chamas
 */

void lerUltravioleta(Data *d, const int p, bool debug)
{
	float v = analogRead(p);

	if (debug == true) {
		Serial.print("DEBUG=Porta analogica: ");
		Serial.println(v);
	}

	d->ultravioleta = v;
}
