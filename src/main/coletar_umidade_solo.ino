/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Funções para ler dados do sensor de umidade do solo
 */

/**
 * Coleta a porcentagem de umidade do solo e salva nos dados a porcentagem.
 *
 * @details @n@n NOTE: Essa função trabalha em volta de um sensor bugado que só retorna no maximo
 * 1023 em vez de 1024, então valores em 1024 são convertidos para 0, e o sensor tambem só
 * retorna os valores de forma invertida com o mais alto sendo mais seco.@n
 * @n
 * O debug imprime a umidade do solo de acordo com o sensor e a porcentagem final.
 *
 * @param d estrutura onde sera salva os dados
 * @param p numero do pino do sensor de umidade do solo
 * @param debug verdadeiro para imprimir mais informações
 */
void lerUmidadeSolo(Data *d, const int p, bool debug)
{
	int umidadeSolo = analogRead(p);

	if (debug == true) {
		Serial.print("DEBUG=Umidade do solo: ");
		Serial.println(umidadeSolo);
	}

	if (umidadeSolo == 1023)
	{
		umidadeSolo = 0.00;
	} else {
		umidadeSolo = (float) 100 - (umidadeSolo / 10.23);
	}

	if (debug == true) {
		Serial.print("DEBUG=Umidade do solo percent: ");
		Serial.println(umidadeSolo);
	}

	d->umidadeSolo = umidadeSolo;
}
