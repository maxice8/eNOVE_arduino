/**
 * @file
 * @author maxice8
 * @date 28/11/2018
 * @brief Funções para lidar com sensor DHT para temperatura
 */

/**
 * Coleta temperatura usando o sensor DHT22
 *
 * @param d estrutura em que a leitura é salva.
 * @param dht objeto que faz interface com o sensor dht.
 * @param debug verdadeiro para imprimir mais informações.
 */
void lerTemperatura(Data *d, DHT dht, bool debug)
{
	float t = dht.readTemperature();

	if (debug == true) {
		Serial.print("DEBUG=Temperatura: ");
		Serial.println(t);
	}

	d->umidade = t;
}
