/**
 * @file
 * @author maxice8
 * @date 23/11/2018
 * @brief arquivo principal do projeto com toda a logica.
 * @details
 * Arquivo com a função loop e main.@n
 * Aqui que vai toda a logica necessaria para fazer uso da smart house.@n
 * Tambem aqui que é declarado as variaveis globais que são passadas para funções.@n
 */

#include <Stepper.h>
#include <SPI.h>
#include <Ethernet.h>
#include "DHT.h"
#include "Tone.h"				/* Biblioteca para tocar tons, qualquer coisa com o buzzer usa aqui */
#include "Time.h"
#include <LiquidCrystal.h>		/* LCD */

/** Usada par ler requests em HTTP */
String readString = String(30);

/** Tipo do DHT */
#define DHTTYPE DHT22

/** Pino do sensor DHT22 */
const int DHTPIN = 4;

/** Objeto DHT */
DHT dht(DHTPIN, DHTTYPE);

/** Pino do buzzer */
const int PIN_SENSOR_BUZZER = 52; /** Pino do buzzer */

/** Pino peque pertence ao sensor LCD */
const int PIN_LCD_0 = 0;
/** Pino peque pertence ao sensor LCD */
const int PIN_LCD_1 = 0;
/** Pino peque pertence ao sensor LCD */
const int PIN_LCD_2 = 0;
/** Pino peque pertence ao sensor LCD */
const int PIN_LCD_3 = 0;
/** Pino peque pertence ao sensor LCD */
const int PIN_LCD_4 = 0;
/** Pino peque pertence ao sensor LCD */
const int PIN_LCD_5 = 0;

/** Objeto que guarda o painel LCD */
LiquidCrystal lcd(PIN_LCD_0, PIN_LCD_1, PIN_LCD_2, PIN_LCD_3, PIN_LCD_4,
				  PIN_LCD_5);

/** Pino do sensor LDR */
const int PIN_LDR_SENSOR = A4;

/** Pino do RGB vermelho */
const int PIN_RED_RGB = 19;
/** Pino do RGB verde */
const int PIN_GREEN_RGB = 18;
/** Pino do RGB azul */
const int PIN_BLUE_RGB = 17;

/** Pino do trigger do sensor de presença */
const int PIN_SENSOR_TRIG = 13;
/** Pino do echo do sensor de presença */
const int PIN_SENSOR_ECHO = 12;

/** Pino para o sensor de presença que sera usado no sistema de alarme */
const int PIN_SENSOR_PRESENCA = 0;

/** Pino do sensor de chuva */
const int PIN_SENSOR_CHUVA = A1;
 
/** Pino do sensor de umidade do solo */
const int PIN_SENSOR_UMIDADE_SOLO = A8;

/** Pino do sensor de chamas */
const int PIN_SENSOR_FOGO = A13;

/** Pino do sensor infravermelho */
const int PIN_SENSOR_ALARME = 0;

/** Pino do sensor Ultravioleta */
const int PIN_SENSOR_UV = A9;

/**
 * @struct data
 * @brief guarda os valores que serão passados para o servidor JAVA
 * @details os valores são guardados em float se precisam de precisão como
 * a umidade do solo e a temperatura e em inteiros se são uma situação de sim ou não
 * com 0 sendo verdadeiro e 1 sendo falso.
 * O uso dessa estrutura é pra ser passado por ponteiro e populado por funções que
 * façam leitura dos sensores, esses dados são passados para o servidor JAVA em um
 * intervalo de 3 segundos
 * @var data::umidade
 * umiadde do ar em porcentagem
 * @var data::temperatura
 * temperatura em celcius
 * @var data::umidadeSolo
 * umidade do solo em porcentagem
 * @var data::ultravioleta
 * Quantidade de raio ultravioleta no index
 * @var data::chuva
 * se esta chovendo ou não, 0 para chuva, 1 para sem chuva
 * @var data::visita
 * se tem alguem na frente da porta da casa
 * @var data::intruso
 * se tem alguem na casa quando o alarme esta ativado
 * @var data::fogo
 * se a casa está pegando fogo
 */
struct data 
{
	float umidade;
	float temperatura;
	float umidadeSolo;
	float ultravioleta;
	int chuva;
	int visita;
	int intruso;
	int fogo;
};

/** Aceite 'Data' como nome para a estrutura 'data' */
typedef struct data Data;

/** Variavel que guarda os valores que serão passados */
Data dados;

/** Se a garagem está aberta */
bool garagemAberta = false;

/** Pino das LEDS dos postes */
const int LED_POSTES = 2;

/** Objeto interface do motor de passos */
Stepper myStepper(500, 8, 10, 9, 11);

/** Objeto interface da campainha */
Tone tone1;

/** Contador de tempo para uma visita */
int tempo = 12;

/** Se o alarma está ativo */
int alarmeAtivo = HIGH;

int campainha = HIGH;

void setup()
{
/*
	Ethernet.begin(mac, ip, gateway, subnet);
	server.begin();
*/

	dht.begin();

	/* Pino sensor de presença como entrada */
	pinMode(PIN_SENSOR_PRESENCA, INPUT);

	/* Pino de input pra chuva */
	pinMode(PIN_SENSOR_CHUVA, INPUT);

	/* Pino de input pra umidade do solo */
	pinMode(PIN_SENSOR_UMIDADE_SOLO, INPUT);

	DDRB = 0x0F;				// Configura Portas D08,D09,D10 e D11 como saída 
	PORTB = 0x00;				// Reset dos bits da Porta B (D08 a D15) 

	/* Pinos para o sensor ultrassonico */
	pinMode(PIN_SENSOR_TRIG, OUTPUT);
	pinMode(PIN_SENSOR_ECHO, INPUT);

	/* Pino usado para sensor de luminosidade (LDR) */
	pinMode(PIN_LDR_SENSOR, INPUT);

	/* 
	 * Biblioteca Tone tem seu propio tipo orientado a objeto para começar
	 * o parametro chamado da função é o pino do buzzer
	 */
	pinMode(PIN_SENSOR_BUZZER, OUTPUT);

	/*
	 * Pino do sensor infra-vermelho
	 */
	pinMode(PIN_SENSOR_ALARME, INPUT);

	/* Pinos que fazem partes das LEDS dos ambientes */
	pinMode(3, OUTPUT);
	pinMode(28, OUTPUT);
	pinMode(31, OUTPUT);
	pinMode(24, OUTPUT);
	pinMode(25, OUTPUT);
	pinMode(26, OUTPUT);
	pinMode(27, OUTPUT);
	pinMode(5, OUTPUT);
	pinMode(34, OUTPUT);

	/* Postes */
	pinMode(2, OUTPUT);

	/* Sensor ultravioleta */
	pinMode(PIN_SENSOR_UV, INPUT);

	/* Iniciali o motor de passos */
	myStepper.setSpeed(60);

	tone1.begin(PIN_SENSOR_BUZZER);

	/* Inicializa a LCD */
	//lcd.begin(16, 2);

	/* Liga a LCD com o texto, eNOVE HOUSE */
	//ligarLCD(lcd);

	/* Ative as RGBs */

	pinMode(PIN_RED_RGB, OUTPUT);
	pinMode(PIN_GREEN_RGB, OUTPUT);
	pinMode(PIN_BLUE_RGB, OUTPUT);

	Serial.begin(115200);
}

void loop()
{

	/* RGB permanece verde enquanto não a leitura de input */
	setColor(0, 255, 0);

	/* Ve se tem visita */
	lerVisita(&dados, PIN_SENSOR_TRIG, PIN_SENSOR_ECHO, false);
	
	if (dados.visita == 0) {
	/* Se visita for verdadeiro a gente entra aqui */
		if (tempo > 10) {
			/* 
			 * Se o tempo for maior que 10 a gente toca a campainha e reseta
			 * o contador de tempo
			 */
			if (campainha == LOW) {
				tocarCampainha(tone1);
			}
			tempo = 0;
		} else {
			/* 
			 * Nos chegamos aqui se o tempo for menor que 10 e a visita
			 * for verdadeira, nós não queremos tocar a campainha mais
			 * de uma vez em um tempo de 10 segundos, então a gente
			 * adiciona 3 ao tempo que é o intervalo que a função é
			 * chamada para checar a visita
			 */
			tempo += 1;
		}
	} else {
		/*
		 * Nós chegamos aqui se não temos visita, nós colocamos o tempo para
		 * um numero acima de 10 para que a proxima vez que checarmos, se tiver
		 * visita nós iremos tocar a campainha
		 */
		tempo = 12;
	}

	delay(1000);
	
/*
	for (int i = 22; i < 56; i++) {
		digitalWrite(i, HIGH);
	}
*/

	/* Le sensor LDR e liga/desliga as luzes de acordo */
	controlarPostes(LED_POSTES, PIN_LDR_SENSOR, false);

	/* Le sensor infravermelho */
	lerAlarme(&dados, PIN_SENSOR_PRESENCA, alarmeAtivo, false);

	if (alarmeAtivo == LOW) {
		if (dados.intruso == 0) {
			tocarCampainha(tone1);
		}
	}

	/* Le sensor de chama */
	lerFogo(&dados, PIN_SENSOR_FOGO, false);

	/* Começe com uma String zerada */
	String str = "";

	while(Serial.available() > 0) {

		/* 
		 * Mude a cor da RGB para vermelho enquanto estamos lidando
		 * com uma request no serial.
		 */
		setColor(255, 0, 0); 

		/* Le o conteudo do Serial em uma string */
		str = Serial.readString();

		/* Ve o tamanho da string mais 1 */
		int len = str.length() + 1;

		/* 30 caracteres + separador nulo */
		char cha[31] = "";

		/* Converta a String em um array de chars */
		str.toCharArray(cha, len);
		
		/* Ponteiro para ser usado em funções */
		const char * ch = cha;

		{
			/* Procure pelo primeiro '=' */
			char * firstParamPos = strchr(ch, '=');

			char * lastPos = strchr(ch, '\0');
			
			char * dividerPos = strchr(ch, '&');
			if (dividerPos == NULL) {
				/* Se não tem chave a gente coloca como o separador final */
				dividerPos = lastPos;
			}

			/* 3 caracteres + separador nulo */
			char pino[4];

			/* 1 caracter + separador nulo */
			char status[2];

			/* 
			 * Tamanho é a posição do espaço no final
			 * menos a posição do parametro de separação
			 * menos 1 que é o parametro em si mesmo.
			 */
			int paramLength = dividerPos - firstParamPos - 1;

			/* +1 para pular o separador */
			memcpy(pino, firstParamPos + 1, paramLength); 
			pino[paramLength] = 0; // separador nulo

			/*
			 * Se o divisor e a ultima posição são difernetes quer dizer
			 * que estamos lidando com uma request de 2 valores.
			 */
			if (dividerPos != lastPos) {
				char * secondParamPos = strchr(dividerPos + 1, '=');

				int secondParamLength = lastPos - secondParamPos - 1;

				memcpy(status, secondParamPos + 1, secondParamLength);
				status[secondParamLength] = 0;
			}

			/* Converta eles para inteiros */
			int pin = atoi(pino);
			int stat = atoi(status);

			/* Pino 101 serve para ativar/desativar o alarme */
			if (pin == 101) {
				alarmeAtivo = stat;
			} else 

			/* Pino 102 serve para pegar dados do sistema dos sensores */
			if (pin == 102) {
				
				/* RGB fica armarela enquanto le sensores */
				setColor(255, 255, 0);

				/* Sensor DHT22 não funciona
				lerUmidade(&dados, dht, false);
				lerTemperatura(&dados, dht, false);
				*/

				lerChuva(&dados, PIN_SENSOR_CHUVA, false);
				lerUmidadeSolo(&dados, PIN_SENSOR_UMIDADE_SOLO, false);
				lerUltravioleta(&dados, PIN_SENSOR_UV, false);

				/* Sensor DHT22 não funciona
				Serial.print("\"UMIDADE\":\"");
				Serial.print(dados.umidade);
				Serial.print("\",\"TEMPERATURA\":\"");
				Serial.print(dados.temperatura);
				*/

				String jstr = "{\"US\":";
				jstr = String(jstr + dados.umidadeSolo);
				jstr = String(jstr + ",\"C\":" + dados.chuva);
				jstr = String(jstr + ",\"V\":" + dados.visita);
				jstr = String(jstr + ",\"F\":" + dados.fogo);
				jstr = String(jstr + ",\"I\":" + dados.intruso);
				jstr = String(jstr + ",\"UV\":" + dados.ultravioleta);
				jstr = String(jstr + ",\"SA\":" + alarmeAtivo + "}");
				Serial.println(jstr);
			} else

			if (pin == 103) {
				setColor(0, 0, 255);
				/* Abre garagem */
				run_stepper(myStepper, false);
			} else 
			
			if (pin == 104)  {
				campainha = stat;
			} else {
				/* Qualquer outro pino a gente muda o status */
				setStatus(stat, pin, false);
			}
		}
	}
}
