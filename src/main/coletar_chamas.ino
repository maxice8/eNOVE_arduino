/**
 * @file
 * @author maxice8
 * @date 30/11/2018
 * @brief funções para lidar com sensor de chamas
 */

/**
 * Le sensor de chamas, guarda valor em estrutura e toca alarme
 *
 * @param *d ponteiro para a estrutura onde sera salvo os dados
 * @param p pino do sensor de chamas
 * @param debug verdadeiro para imprimir mais informações
 */
void lerFogo(Data *d, const int p, bool debug)
{
	int v = analogRead(p);

	if (debug == true) {
		Serial.print("DEBUG=Porta analogica: ");
		Serial.println(v);
	}

	if (v > 800) {
		if (debug == true)
			Serial.println("DEBUG=Tá pegando fogo");
		d->fogo = 0;
	} else {
		if (debug == true)
			Serial.println("DEBUG=Não tá pegando fogo");
		d->fogo = 1;
	}

}
