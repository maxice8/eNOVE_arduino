/**
 * @file
 * @author maxice8
 * @date 28/11/2018
 * @brief Funções para lidar com sensor DHT para umidade
 */

/**
 * Coleta umidade usando o sensor DHT22
 *
 * @param d estrutura em que a leitura é salva.
 * @param dht objeto que faz interface com o sensor dht.
 * @param debug verdadeiro para imprimir mais informações.
 */
void lerUmidade(Data *d, DHT dht, bool debug)
{
	float u = dht.readHumidity();

	if (debug = true) {
		Serial.print("DEBUG=Humidade: ");
		Serial.println(u);
	}

	d->umidade = u;
}
