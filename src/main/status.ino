/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Funções para manipular status dos ambientes
 * @details
 * Ambientes são entendidos como qualquer lugar/sistema/objeto que pode ter@n
 * seu status alternato entre 2 statos, ativo ou desativo.@n
 * Exemplos: luzes de um comodo, sistema de alarme, porta da garegem.@n
 * Cada ambiente possue seu propio numero inteiro para uso interno@n
 * os valores estão disponiveis em Ambientes.md.
 */

/**
 * Coloque o ambiente atual no estado pedido
 *
 * @details
 * O debug imprime quantos elementos tem no array, o ambiente e seu status@n
 * e se o ambiente já estiver com o status que foi requerido é imprimido@n
 * uma mensagem.
 *
 * @param s status que deve ser usado
 * @param p pino que deve ser seu status mudado
 * @param debug verdadeiro para imprimir mais informações
 */
void setStatus(int s, int p, bool debug)
{

	if (s == digitalRead(p)) {
		if (debug == true) {
			Serial.print("DEBUG=Pino: ");
			Serial.print(p);
			Serial.println(" já esta no status requerido");
		}
	}

	if (debug == true) {
		Serial.println("");
		Serial.print("DEBUG=Pino: ");
		Serial.println(p);
		Serial.print("DEBUG=Status: ");
		Serial.println(s);
		Serial.println("");
	}

	digitalWrite(p, s);

}
