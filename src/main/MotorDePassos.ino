/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Arquivo com funções para manipulação do motor de passos.
 */

/**
 * Gira o motor 90 graus horario depois 120 no anti-horario
 *
 * @details
 * Esta função demora bastante tempo então não deve ser chamada em excesso@n
 * se demora 4 segundos para completar no estado atual.@n
 * @n
 * O debug imprime quanto graus e em que sentido esta girando.
 *
 * @param s Variavel com o objeto que representa o motor de passos
 * @param debug verdadeiro para imprimir mais informações
 */

/* TODO: implement debug */
void run_stepper(Stepper s, bool debug) { 
   //Gira o motor no sentido horario a 90 graus
    s.step(-1060); 
  
   delay(4000);
   //Gira o motor no sentido anti-horario a 120 graus
   s.step(1060);
}

