/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Arquivo para operação de das LEDS RGBs.
 * @details
 * Arquivo contem função para manipular as cores usadas na LEDS RGBs.
 */

/**
 * Muda a cor dos pinos RGB
 *
 * @param redValue Valor de 0 a 255 do vermelho
 * @param greenValue Valor de 0 a 255 do verde
 * @param blueValue Valor de 0 a 255 do azul
 */
void setColor(const int redValue, const int greenValue, const int blueValue) {
	analogWrite(PIN_RED_RGB, redValue);
	analogWrite(PIN_GREEN_RGB, greenValue);
	analogWrite(PIN_BLUE_RGB, blueValue);
}

