/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Funções para lidar com postes
 */

/**
 * Le sensor de LDR e liga ou desliga os postes.
 *
 * @details
 * Para ser ligado o valor da leitura digital deve ser maior que 1000.@n
 * @n
 * O debug imprime a luminosidade de acordo com o sensor e quais postes@n
 * estão sendo ligados ou desligados.
 *
 * @param l LED dos postes para serem ligadas ou desligadas
 * @param p pino do sensor LDR
 * @param debug verdadeiro para imprimir mais informações
 */
void controlarPostes(const int l, const int p, bool debug)
{
	int analogValue = analogRead(p);

	if (debug == true) {
		Serial.print("DEBUG=Luminosidade: ");
		Serial.println(analogValue);
	}

	if (analogValue > 900) {
		if (debug == true) {
			Serial.print("DEBUG=Ligando postes. (pino ");
			Serial.print(l);
			Serial.println(")");
		}
		digitalWrite(l, HIGH);
	} else {
		if (debug == true) {
			Serial.print("DEBUG=Desligando postes. (pino  ");
			Serial.print(l);
			Serial.println(")");
		}
		digitalWrite(l, LOW);
	}
}
