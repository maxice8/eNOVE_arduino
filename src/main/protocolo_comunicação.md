PROTOCOLO DE COMUNICAÇÂO
========================
 
### Definição

O protocolo de comunicação media a troca de dados esperadas pelo WebServer
e o Arduino.

#### Dados de monitoramento

A cada N segundos o Arduino deve mandar para o WebServer os dados coletados
dos sensores no formato JSON.

Exemplo:

    { 
		"CHUVA": 0,
		"UMIDADE": 60.5,
		"VISITA": 1,
		"INTRUSO": 0
	}

#### Ações

Quando o usuario pedir para fazer uma ação o WebServer ira mandar uma string
de request no formato

    AMBIENTE=[N]&STATUS=[0|1]

Exemplo:

	# Desliga as luzes no ambiente 1
	AMBIENTE=1&STATUS=0
	
	# Coleta dados dos sensores
	AMBIENTE=11

As chaves se referem a:

- `AMBIENTE` qual o local da ação que deve ser feita.
- `STATUS` se a ação deve ser de ligar ou desligar.
