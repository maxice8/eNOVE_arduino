/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Arquivo com funções para coletar dados com o sensor de chuva.
 */

/**
 * Le o sensor de chuva e retorna 0 se tem chuva e 1 se não
 *
 * @details
 * O debug imprime se tem chuva ou não de acordo com o sensor.
 *
 * @param d estrutura em que sera mantido o valor da leitura
 * @param p pino do sensor de chuva
 * @param debug verdadeiro para imprimir mais informações
 */
void lerChuva(Data *d, const int p, bool debug)
{
	int chuva = analogRead(p);

	if (debug == true) {
		Serial.print("DEBUG=Chuva: "); 
		Serial.println(chuva);
	}

	if (chuva < 800) {
		d->chuva = 0;
	} else {
		d->chuva = 1;
	}
}
