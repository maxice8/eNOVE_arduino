/**
 * @file
 * @author maxice8
 * @date 24/11/2018
 * @brief Funções para lidar com o sensor de visitas.
 */

/**
 * @brief toca notas para uma campainha usando um objeto tipo Tone.
 *
 * @param tone1 objeto que controla a campainha
 */
void tocarCampainha(Tone tone1)
{
	/* XXX: Editar isso para tocar uma campainha decente */
	tone1.play(NOTE_A4);
	delay(100);
	tone1.play(NOTE_D8);
	tone1.stop();
}

/**
 * @brief Usa sensor ultrassonica para checar por visitas
 *
 * @details
 * Usa sensor ultrassonico para checar se tem objetos a menos de 10 centimetros@n
 * salva o resultado na estrutura de dados.@n@n
 * Essa função tem um delay de 10 microsegundos.@n
 * @n
 * O debug imprime a distancia que o sensor detecta o primeiro objeto.
 *
 * @param d estrutura de dados onde são salvo os valores
 * @param t pino trigger do sensor
 * @param e pino echo do sensor
 * @param debug verdadeiro para imprimir mais informações
 */
void lerVisita(Data *d, const int t, const int e, bool debug)
{
	double distanceCentimeter;
	int pulseLenMicroseconds;

	digitalWrite(t, HIGH);
	delayMicroseconds(10);
	digitalWrite(t, LOW);

	pulseLenMicroseconds = pulseIn(e, HIGH);

	distanceCentimeter = pulseLenMicroseconds * 0.034 / 2;

	if (debug) {
		Serial.print("DEBUG=distancia: ");
		Serial.println(distanceCentimeter);
	}

	if (distanceCentimeter < 9) {
		d->visita = 0;
	} else {
		d->visita = 1;
	}
}

/**
 * Leia alarme do sensor infravermelho e guarda se existe intruso ou não
 *
 * @details
 * O debug imprime se o alarme estiver desativado e o valor do resultado@n
 * do sensor.
 *
 * @param d estrutura onde são salvos os dados
 * @param p pino do sensor infravermelho
 * @param a status do alarme, ativo ou desativo
 * @param debug verdadeiro para imprimir mais informções
 */
void lerAlarme(Data *d, const int p, int a, bool debug)
{
	int presenca = digitalRead(p);

	if (debug == true)
		Serial.print("DEBUG=Intruso: ");

	if (presenca == LOW) {
		d->intruso = 1;
	} else {
		d->intruso = 0;
	}

	if (debug == true)
		Serial.println(presenca);
}
