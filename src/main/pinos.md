PINOS
=====

### Definição

Este documento define os pinos fisicos do arduino a sua localização

### Tabela

AMBIENTE		| TIPO		| PINO
:------:		| :--:		| :--:
SALA A			| LED		| 3
SALA B			| LED		| 3
BANHEIRO A		| LED		| 28
BANHEIRO B		| LED		| 31
QUARTO A		| LED		| 24
QUARTO B		| LED		| 25
ESCRITORIO		| LED		| 26
CORREDOR		| LED		| 27
GARAGEM			| LED		| 5
CHURRASCO		| LED		| 34
TRIGGER 		| DIGITAL	| 13
ECHO			| DIGITAL	| 12
DHT22			| DIGITAL	| 4
INFRAVERMELHO	| DIGITAL	| 7
CHUVA			| ANALOGICO	| A1
LDR				| ANALOGICO | A4
UMIDADE DO SOLO | ANALOGICO | A8
ULTARVIOLETA	| ANALOGICO	| A9
SENSOR CHAMAS	| ANALOGICO | A13
VERMELHO		| RGB		| 19
VERDE			| RGB		| 18
AZUL			| RGB		| 17
POSTES			| LEDS		| 2
MOTOR DE PASSOS	| STEPPER	| 8 10 9 11

### Lista de Pinos defeituosos

- 20 (5V)
- 21 (5V)
- 22 (Fraco)
- 23 (Fraco)
- 35 (Fraco)
- 37 (Fraco)
- 39 (Fraco)
- 38 (Fraco)
- 29 (Fraco)
